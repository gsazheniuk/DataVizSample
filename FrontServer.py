import tornado.escape
import tornado.ioloop
import tornado.web
from tornado import gen
import os.path


class RootHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        self.render(os.path.join("html", "default.html"), messages=[])
        pass


class PrintHandler(tornado.web.RequestHandler):
    def get(self):
        print(self.request.body)
        self.write(b'{}')
        pass

    def post(self):
        print(self.request.body)
        self.write(b'{}')
        pass


class FrontWatchServer:
    def __init__(self):
        self.app = tornado.web.Application(
            [
                (r"/", RootHandler),
                (r"/print", PrintHandler),
                # (r"/default.html", RootHandler),
                # Data Handlers
                # Static resources handlers
                (r"/css/(.*)", tornado.web.StaticFileHandler, {"path": "static\\css\\"}),
                (r"/js/(.*)", tornado.web.StaticFileHandler, {'path': 'static\\js\\'}),
                (r"/data/(.*)", tornado.web.StaticFileHandler, {"path": "static\\data\\"}),
            ],
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "static"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=False,
            debug=True,
        )
        pass

    def run(self):
        self.app.listen(8000)
        tornado.ioloop.IOLoop.current().start()
        pass
